<?php

namespace App\Frontend\Modules\Users;

use \Entity\Users;
use \OCFram\BackController;
use \OCFram\HTTPRequest;
use \Form\FormHandler;
use \Form\FormBuilder\RegisterFormBuilder;

class UsersController extends BackController
{
    public function executeRegister(HTTPRequest $request)
    {
        $this->processFormRegister($request);

        $this->page->addVar('title', 'Inscription d\'un utilisateur');
    }


    /**
     * FormHandler for the user register
     * @param HTTPRequest $request
     */
    public function processFormRegister(HTTPRequest $request)
    {
        if( $request->method() == 'POST' )
        {
            $user = new Users( array(
               'username' => $request->postData( 'username' ),
               'password' => $request->postData( 'password'),
               'email' => $request->postData( 'email'),
           ));

            // On vérifie que le username est disponible
            $userBDD = $this
				->managers
				->getManagerOf('Users')
                ->findByUsername(
                	$request->postData('username')
				);

            $username = $request->postData('username');

			// Si le username n'existe pas en BDD
            if(isset($userBDD) && !empty($userBDD))
            {
				$erreurs = 'Ce nom d\'utilisateur n\'est plus disponible !';
				$this->app->httpResponse()->redirect( '/admin/user-register.html' );

			}
            else // Le username est présent dans la BDD
            {
				if($user->isNew())
				{
					// On force le role utilisateur à USER
					$user->setRole('USER');
					// On génère une clé de salage
					$user->setSalt(substr(md5(time()), 0, 23));
				}

				$mdpForm = $request->postData('password');

				if(isset($mdpForm) && !empty($mdpForm) )
				{
					$pass = sha1($mdpForm . $user->salt());
					$user->setPassword($pass);
				}
				else
				{
					$erreurs = 'Veuillez entrez un mot de passe valide !';
					$this->app->httpResponse()->redirect( '/admin/user-insert.html' );
				}
            }
        }
        else
        {
            $user = new Users();
        }


        $formBuilder = new RegisterFormBuilder($user);
        $formBuilder->build();
        $form = $formBuilder->form();

        $formHandler = new FormHandler($form, $this->managers->getManagerOf('Users'), $request);

        if ($formHandler->process())
        {
            $this->app->user()->setFlash('Félicitation vous faite maintenant partit de l\'aventure !');

            // Redirection vers la page d'accueil
            $this->app->httpResponse()->redirect('/');
        }

        // Si une erreur a été générer, on l'envoie à la page
        if(isset($erreurs)) {
            $this->page->addVar( 'erreurs', $erreurs );
        }

//        $this->page->addVar( 'user', $user );

        // On envoie le formulaire à la page
        $this->page->addVar('form', $form->createView());


    } /* End of ProcessFormUser */

}
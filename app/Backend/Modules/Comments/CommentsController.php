<?php
namespace App\Backend\Modules\Comments;

use \Entity\Comments;
use \Form\FormBuilder\UpdateCommentsFormBuilder;
use \OCFram\HTTPRequest;
use \Form\FormHandler;
use \OCFram\BackController;

class CommentsController extends BackController
{
    // UPDATE A COMMENT //
    public function executeUpdate(HTTPRequest $request)
    {
        $this->processForm($request);

        $this->page->addVar('title', 'Modification d\'un commentaire');
    }

    // DELETE A COMMENT
    public function executeDelete(HTTPRequest $request)
    {
        $this->managers->getManagerOf('Comments')->delete($request->getData('id'));

        $this->app->user()->setFlash('Le commentaire a bien été supprimé !');

        $this->app->httpResponse()->redirect('/admin/home.html');
    }



    // FormHandler for Update a comment
    public function processForm(HTTPRequest $request)
    {
        if ($request->method() == 'POST')
        {
            $comment = new Comments([
                'chapter' => $request->postData('chapter'),
                'flag' => $request->postData('flag'),
                'author' => $request->postData('author'),
                'content' => $request->postData('content'),
            ]);

            if( $request->getExists( 'id' ) ) {
                $comment->setId( $request->getData( 'id' ) );
            }

        }
        else
        {
            if ($request->getExists('id'))
            {
                $comment = $this->managers->getManagerOf('Comments')->find($request->getData('id'));
            }
            else
            {
                $comment = new Comments();
            }
        }

        $formBuilder = new UpdateCommentsFormBuilder($comment);
        $formBuilder->build();
        $form = $formBuilder->form();
        $formHandler = new FormHandler($form, $this->managers->getManagerOf('Comments'), $request);

        if ($formHandler->process()) {
            $this->app->user()->setFlash( 'Le commentaire a bien été modifié !');
            $this->app->httpResponse()->redirect('/admin/home.html');
        }

        $this->page->addVar('comment', $comment);
        // On passe le formulaire généré a la vue
        $this->page->addVar('form', $form->createView());
    }

}
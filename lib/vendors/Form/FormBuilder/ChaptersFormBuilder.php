<?php
namespace Form\FormBuilder;

use \Form\Field\StringField;
use \Form\Field\TextField;
use \Form\Validators\MaxLengthValidator;
use \Form\Validators\NotNullValidator;

class ChaptersFormBuilder extends FormBuilder
{
    public function build()
    {
        $this->form->add(new StringField([
                  'label' => 'Auteur',
                  'name' => 'author',
                  'value' => 'Jean Forteroche',
                  'disabled' => 'disabled',
                  'maxlength' => 100,
                  'validators' => [
                      new MaxLengthValidator('Le titre spécifié est trop long (100 caractères maximum)', 100),
                      new NotNullValidator('Merci de spécifier le contenu du chapitre'),
                  ],
              ]))
            ->add(new StringField([
                'label' => 'Titre :',
                'name' => 'title',
              'maxlength' => 100,
                'validators' => [
                    new MaxLengthValidator('Le titre spécifié est trop long (100 caractères maximum)', 100),
                    new NotNullValidator('Merci de spécifier le titre du chapitre'),
                ],
            ]))
            ->add(new TextField([
              'label' => 'Contenu :',
              'name' => 'content',
                'rows' => 8,
                'cols' => 220,
              'validators' => [
                  new NotNullValidator('Merci de spécifier le titre du chapitre'),
              ],
          ]));
    }
}
<?php
namespace Form\Validators;

class NotNullValidator extends Validator
{
    public function isValid($value)
    {
        return $value != '';
    }
}
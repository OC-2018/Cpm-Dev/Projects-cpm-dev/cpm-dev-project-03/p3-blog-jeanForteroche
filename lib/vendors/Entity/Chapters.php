<?php

namespace Entity;

use \OCFram\Entity;

/**
 * Class Chapters
 *
 */
class Chapters extends Entity
{
    protected $id;
    protected $title;
    protected $content;
    protected $author;
    protected $dateCreate;
    protected $lastModif;

    const INVALID_AUTHOR = 1;
    const INVALID_TITLE = 2;
    const INVALID_CONTENT = 3;

    public function isValid()
    {
        return !( empty($this->title) || empty($this->content) || empty($this->author));
    }

    // GETTERS //
    public function id()
    {
        return $this->id;
    }

    public function title()
    {
        return $this->title;
    }

    public function content()
    {
        return $this->content;
    }

    public function author()
    {
        return $this->author;
    }

    public function dateCreate()
    {
        return $this->dateCreate;
    }

    public function lastModif()
    {
        return $this->lastModif;
    }

    // SETTERS //
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setTitle($title)
    {
        if (!is_string($title) || empty($title))
        {
            $this->erreurs[] = self::INVALID_TITLE;
        }

        $this->title = $title;
    }

    public function setContent($content)
    {
        if (!is_string($content) || empty($content))
        {
            $this->erreurs[] = self::INVALID_CONTENT;
        }
        $this->content = $content;
    }

    public function setAuthor($author)
    {
        if (!is_string($author) || empty($author))
        {
            $this->erreurs[] = self::INVALID_AUTHOR;
        }
        $this->author = $author;
    }

    public function setDateCreate(\DateTime $dateCreate)
    {
        $this->dateCreate = $dateCreate;
    }

    public function setLastModif(\DateTime $lastModif)
    {
        $this->lastModif = $lastModif;
    }


}